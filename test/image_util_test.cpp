#include <gtest/gtest.h>

#include "image_util.hpp"

namespace {
    const std::string valid_image_encoded = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYFBgYGBwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgr/2wBDAQICAgICAgUDAwUKBwYHCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgr/wAARCABGADwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD8dPAWikgHZ27jvXovh7SLua9EENs8rHoFXJ4rL8IaaIrYMkAA5+bFdv4Wiv7eZJLO+ns2mDwtc25IZQ4KsRyP4SfqOK8OrNTqa7Hs4ek4U9FsaXhewvb+48oJtRj0xg4rr7DQbeaPIhIJ+VTjvWH4k1rxb4b8WQ6n45tJpI7+ISzamTvdZCgZmuMcFsctJ1PDvuJeQ9R/wlfhHS4kl1jXraESRxm33SDLbwCCAOSMMDn056c1y4vDYijV5bXXTzO7CYnD1qXNez6p9CpNZSRD7PJ/yzbnAqp4h8NXljCWu7ExttjZkfAdVkjWSNmXqoeNg6EgblORkUz43/EHRvAmhyLo+q20+u6jJFBpunH5nJkVW851x8saqyn5vvl0Cgjcy6GieEtf8AeD7OHxWtxPd+IbW11fUb+8LM0km664ySecS7mGerLwDnPRRwE1hZ1qt1bb7zmrY6EsXChSs77vtoebeMtEhkhcSRjGMivGvEXh6AarIEAx7mvfPGl7YmGSFf4s7SPSvH9djjGpOzr97kfStsNJo48dCLO58NWUNxDHb24+TBJYjvmu58NWMFnAlrdEOBJlQOwx1rnPCMQjQQeTgcBAB1P/AOuu98PaPDeu0LFVIjOSTjPHb1rz689T18LC6Jtc1HRvEult4V1G6uNOvBbxva6lLqbKoRNoEgRyAWVwwDLkfL85AXK2dT+BB8Xrp+pyqv8Abep3UVlNP9jA8iYRHyvMhBDRjbBKvz87Y3K/d+bvfG37N/xLu/hx4K8S+DvAk+rS+JZPItdM0yFptQvys0oit4olQs5ZnBVEyX8xsAkGsTRfDXiC2/ZePxsvtSkutGAv9V0y8ntXRrl/J1mxe7UEhgGuYXVXYhlaIkKRHhvtMDH9xFVN2k/w/wCHPjMwnD63JReibPnX4ieJDqni+XVvFWktbNpc1pDp08X+jOLdSp+VNmNhVy2S4UFE5+bLekXnxNtvEnw9s/F+p2b2TzWmGhkbJxHmJT0H8CJ65OTnmuW8d/DXUvF3wHvfjdqVzqE2hHULnTrTWZ4YYree7gtrWd4Qsah4wPt9lMJWDRSfaCULHeD5H49+JWrDwZpPhWXdC9rYiGSJj80YjYx7D3yNuOeeK4c05p0VGL0b/K/6m+U1YU6rk+i0/A1fFvxQhvrphaSAIp6Vx+p+MWuroy7x0AFchc39wsm8OSCOaZDHd3i+eoJGccVw0qSgjprV5TZ9aeFDbXDCFgishycdf/r12mharp+ko1xdRrg5MbE8DAxn8wa+R3+OPjSbULZrS9Wzj3E5i7D0LEc8d/cVo618SPHcukgWOsyn7MjfMZAFcbS5BB9g2O5IOORg4yyurLVs66eeUIOyTP2F+F3h34l+O/2O9L8bfBvxGbDxLplvHFpckN/9iMUhZfLlF0zQraFZCCLjzQYWUMGiysh+lvG37Dtl48/YNtfhd+0Dr3hb4PwWKpNa+F/BWgDUtP8AD8kmptJcJHHZOp8uSacTlRI5DTuEWBFCV8U/8Eq/2svB/j79mYeF7eO/gn0a4W2vNQ1JLYQafd5v7m3i+0zKq+c8UTNEFMbSyRSRiJj5cq/eXhJF079la68P+GfGNrFqtlHdwTwwgF5YIpRmRo2BZGKsjEHDL5q7l5Gf1fh/I8JmeHhWlPWLjBpb2ktXeztta9n8tT8P494xxnD2MVOjSTdSnUnGT25ouPu2vHmvdOykmrdb6fmn/wAFLfgxq/7If7E/g34In4h/DrxEmoSINZ1LwFqyTvc3X2Cwt0uZYCkBgjks9OtVIjjILWCySnz5Wkk/MrxmJ4L5rScwebbu8Uq2s6yRbkOwlXRmVwdudwYg5yCQa/XT9oP9nrxD+0H8QNd0b4VfCaDXb3w/Yyajp8GqzC7TS3W5t7eK5UROsN5J50sz+VNDNatBE6Om4+dF+XP7VXwhi+BfxW1L4QrfvPqHh9xbazEkblYJQFUs7OqEMzknaVwodF3MxYL4PF2S0Mjx31TD3cIqPRLVrma003Z9B4dcRYjifIKeYYiChKblpG9rKVk9bPo90vQ8tmlkkcKjHJPIFek+D/AwbQopLncrOScBM1xXgzRH1jXY1EZK78yemK9yjt7nw/GumLZiRVUFXBByCPc18dUko2R+iUYc6cmeCTvHNCZElAZUxGwUjIJPy+xHNdvbarf6Lo+sXFtI6me0+yLIkjJJGki7SQVOSGDmLHI2SOD144a0it7+2ijJcbzlmjUllI54x2IGfbaPx6Gy16S302HUrm0WaWx1QXFxZ3UhCSxqoYqSCHJYpyQRgEYxgGvXtqeCj0H4SfFi7/Zz0vUtM8P/ANmr4judQggutU0x7a+W3hiVpfMWUedHI0huNoMEkYQ6fGSCzZH6y/sjf8FOf2ef2xPCmkfDT9pTxhqHwy+Iup2baXrfi3SNEgbQNchYxJDcTxq2+2uRuZjGqiANG53pFtt4/wAU/DN1puoPBFA7zxhIzIZYERvNMahgSCSVUoQuTjABwuSo7TwJf3lnZgiYF4GUorZDAE9iOp4wc9xXp5bmuNyqsqmGnyv8H6p3X4HlZ1w9lHEWF+rY+kpxW17pp7aNWautHZ6rQ/qq8dfB74UfsG/s86zc/D2xsBp5Lz+JZ9chM97qMO0KJUmQgo0abVVW3LsVTkPudv5SP2wPipq/xq+O3iH4seKdVuLvV9c1WaadpdK+yqkIkZYQA08rghFVQrsxCgDcdua+/vC//BSP9qv9pH4AzfszfEfxLcaxo2mQFNI1WS6WS5iSKPHk+ZuHmfISc8sQQhyuwL+bfxojttN8SjSFt2WWxVrSXzLcRM5R2HzKOh6fl3rHNMXVxi5qs3KTd23u2duU5fh8tpqjh6ahTirRjHZJf1q9311N34CR29xfyyXcRxjsOmc/zrs9d8Ww2WoNamBXVF+Qu2SFPIH61zXwk0S+0Xw+PELW4UGNmWUN6cAEemTTdauLeXUZJmn2sxyw6c9Pxr5m3NUZ9UnyUEed6BO7wojkAFdgKryBnPB9c1bVheyvPdxptaCQNHGMAtsfk+oyvTtx6Yoor2vso+dekyv4ZuZY7qUyvlmkyxAxzhjx6c13skrPI0wcqVjV1CgcHP68gnn24ooqS1uz1j4A3t5otzf6wtwRcQxEweUMLkbiAc/XGcZA5HofDf2lo45fjbrIiVwjzibbJKXOZAHOWwCSd2eee2TjNFFaVv8Ad0On/G/ryOssb7y/BtvaQQbI47bfIA5y5UAj8Pb2FcbFqhnBlmJBZiQqrkAHkDrRRXlUUnc9WvJqyP/Z";
    const std::string corruped_image_encoded = "/9j/4AAQSCORRUPTED/2wBDAQICAgICAgUDAwUKBwYHCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgr/wAARCABGADwDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD8dPAWikgHZ27jvXovh7SLua9EENs8rHoFXJ4rL8IaaIrYMkAA5+bFdv4Wiv7eZJLO+ns2mDwtc25IZQ4KsRyP4SfqOK8OrNTqa7Hs4ek4U9FsaXhewvb+48oJtRj0xg4rr7DQbeaPIhIJ+VTjvWH4k1rxb4b8WQ6n45tJpI7+ISzamTvdZCgZmuMcFsctJ1PDvuJeQ9R/wlfhHS4kl1jXraESRxm33SDLbwCCAOSMMDn056c1y4vDYijV5bXXTzO7CYnD1qXNez6p9CpNZSRD7PJ/yzbnAqp4h8NXljCWu7ExttjZkfAdVkjWSNmXqoeNg6EgblORkUz43/EHRvAmhyLo+q20+u6jJFBpunH5nJkVW851x8saqyn5vvl0Cgjcy6GieEtf8AeD7OHxWtxPd+IbW11fUb+8LM0km664ySecS7mGerLwDnPRRwE1hZ1qt1bb7zmrY6EsXChSs77vtoebeMtEhkhcSRjGMivGvEXh6AarIEAx7mvfPGl7YmGSFf4s7SPSvH9djjGpOzr97kfStsNJo48dCLO58NWUNxDHb24+TBJYjvmu58NWMFnAlrdEOBJlQOwx1rnPCMQjQQeTgcBAB1P/AOuu98PaPDeu0LFVIjOSTjPHb1rz689T18LC6Jtc1HRvEult4V1G6uNOvBbxva6lLqbKoRNoEgRyAWVwwDLkfL85AXK2dT+BB8Xrp+pyqv8Abep3UVlNP9jA8iYRHyvMhBDRjbBKvz87Y3K/d+bvfG37N/xLu/hx4K8S+DvAk+rS+JZPItdM0yFptQvys0oit4olQs5ZnBVEyX8xsAkGsTRfDXiC2/ZePxsvtSkutGAv9V0y8ntXRrl/J1mxe7UEhgGuYXVXYhlaIkKRHhvtMDH9xFVN2k/w/wCHPjMwnD63JReibPnX4ieJDqni+XVvFWktbNpc1pDp08X+jOLdSp+VNmNhVy2S4UFE5+bLekXnxNtvEnw9s/F+p2b2TzWmGhkbJxHmJT0H8CJ65OTnmuW8d/DXUvF3wHvfjdqVzqE2hHULnTrTWZ4YYree7gtrWd4Qsah4wPt9lMJWDRSfaCULHeD5H49+JWrDwZpPhWXdC9rYiGSJj80YjYx7D3yNuOeeK4c05p0VGL0b/K/6m+U1YU6rk+i0/A1fFvxQhvrphaSAIp6Vx+p+MWuroy7x0AFchc39wsm8OSCOaZDHd3i+eoJGccVw0qSgjprV5TZ9aeFDbXDCFgishycdf/r12mharp+ko1xdRrg5MbE8DAxn8wa+R3+OPjSbULZrS9Wzj3E5i7D0LEc8d/cVo618SPHcukgWOsyn7MjfMZAFcbS5BB9g2O5IOORg4yyurLVs66eeUIOyTP2F+F3h34l+O/2O9L8bfBvxGbDxLplvHFpckN/9iMUhZfLlF0zQraFZCCLjzQYWUMGiysh+lvG37Dtl48/YNtfhd+0Dr3hb4PwWKpNa+F/BWgDUtP8AD8kmptJcJHHZOp8uSacTlRI5DTuEWBFCV8U/8Eq/2svB/j79mYeF7eO/gn0a4W2vNQ1JLYQafd5v7m3i+0zKq+c8UTNEFMbSyRSRiJj5cq/eXhJF079la68P+GfGNrFqtlHdwTwwgF5YIpRmRo2BZGKsjEHDL5q7l5Gf1fh/I8JmeHhWlPWLjBpb2ktXeztta9n8tT8P494xxnD2MVOjSTdSnUnGT25ouPu2vHmvdOykmrdb6fmn/wAFLfgxq/7If7E/g34In4h/DrxEmoSINZ1LwFqyTvc3X2Cwt0uZYCkBgjks9OtVIjjILWCySnz5Wkk/MrxmJ4L5rScwebbu8Uq2s6yRbkOwlXRmVwdudwYg5yCQa/XT9oP9nrxD+0H8QNd0b4VfCaDXb3w/Yyajp8GqzC7TS3W5t7eK5UROsN5J50sz+VNDNatBE6Om4+dF+XP7VXwhi+BfxW1L4QrfvPqHh9xbazEkblYJQFUs7OqEMzknaVwodF3MxYL4PF2S0Mjx31TD3cIqPRLVrma003Z9B4dcRYjifIKeYYiChKblpG9rKVk9bPo90vQ8tmlkkcKjHJPIFek+D/AwbQopLncrOScBM1xXgzRH1jXY1EZK78yemK9yjt7nw/GumLZiRVUFXBByCPc18dUko2R+iUYc6cmeCTvHNCZElAZUxGwUjIJPy+xHNdvbarf6Lo+sXFtI6me0+yLIkjJJGki7SQVOSGDmLHI2SOD144a0it7+2ijJcbzlmjUllI54x2IGfbaPx6Gy16S302HUrm0WaWx1QXFxZ3UhCSxqoYqSCHJYpyQRgEYxgGvXtqeCj0H4SfFi7/Zz0vUtM8P/ANmr4judQggutU0x7a+W3hiVpfMWUedHI0huNoMEkYQ6fGSCzZH6y/sjf8FOf2ef2xPCmkfDT9pTxhqHwy+Iup2baXrfi3SNEgbQNchYxJDcTxq2+2uRuZjGqiANG53pFtt4/wAU/DN1puoPBFA7zxhIzIZYERvNMahgSCSVUoQuTjABwuSo7TwJf3lnZgiYF4GUorZDAE9iOp4wc9xXp5bmuNyqsqmGnyv8H6p3X4HlZ1w9lHEWF+rY+kpxW17pp7aNWautHZ6rQ/qq8dfB74UfsG/s86zc/D2xsBp5Lz+JZ9chM97qMO0KJUmQgo0abVVW3LsVTkPudv5SP2wPipq/xq+O3iH4seKdVuLvV9c1WaadpdK+yqkIkZYQA08rghFVQrsxCgDcdua+/vC//BSP9qv9pH4AzfszfEfxLcaxo2mQFNI1WS6WS5iSKPHk+ZuHmfISc8sQQhyuwL+bfxojttN8SjSFt2WWxVrSXzLcRM5R2HzKOh6fl3rHNMXVxi5qs3KTd23u2duU5fh8tpqjh6ahTirRjHZJf1q9311N34CR29xfyyXcRxjsOmc/zrs9d8Ww2WoNamBXVF+Qu2SFPIH61zXwk0S+0Xw+PELW4UGNmWUN6cAEemTTdauLeXUZJmn2sxyw6c9Pxr5m3NUZ9UnyUEed6BO7wojkAFdgKryBnPB9c1bVheyvPdxptaCQNHGMAtsfk+oyvTtx6Yoor2vso+dekyv4ZuZY7qUyvlmkyxAxzhjx6c13skrPI0wcqVjV1CgcHP68gnn24ooqS1uz1j4A3t5otzf6wtwRcQxEweUMLkbiAc/XGcZA5HofDf2lo45fjbrIiVwjzibbJKXOZAHOWwCSd2eee2TjNFFaVv8Ad0On/G/ryOssb7y/BtvaQQbI47bfIA5y5UAj8Pb2FcbFqhnBlmJBZiQqrkAHkDrRRXlUUnc9WvJqyP/Z";
    const std::string valid_image_decoded = crow::utility::base64decode(valid_image_encoded);
} // anonymous namespace


TEST(base64_to_cv2image, empty) {
    const std::string src_img = "";
    EXPECT_THROW(base64_to_cv2img(src_img), std::runtime_error);
}

TEST(base64_to_cv2image, invalid_src) {
    const std::string src_img = " ";
    EXPECT_THROW(base64_to_cv2img(src_img), std::runtime_error);
}

TEST(base64_to_cv2image, corrupted_image) {
    const std::string src_img = corruped_image_encoded;
    EXPECT_THROW(base64_to_cv2img(src_img), std::runtime_error);
}

TEST(base64_to_cv2image, valid_image) {
    const std::string src_img = valid_image_encoded;
    cv::Mat img;
    EXPECT_NO_THROW(img = base64_to_cv2img(src_img));
    EXPECT_FALSE(img.empty());
}

TEST(cv2image_to_base64, empty) {
    const cv::Mat img;
    EXPECT_THROW(cv2img_to_base64(img), std::runtime_error);
}

TEST(cv2image_to_base64, valid_image) {
    const std::vector<uchar> data(valid_image_decoded.begin(), valid_image_decoded.end());
    const cv::Mat img = cv::imdecode(cv::Mat(data), cv::IMREAD_COLOR);
    std::string encoded_image;
    EXPECT_NO_THROW(encoded_image = cv2img_to_base64(img));
    ASSERT_NE(valid_image_encoded, encoded_image);  // encoding image through cv isn't linear 
}

TEST(resize_image, empty_src) {
    const std::string src_img = "";
    EXPECT_THROW({
        try {
            resize_image(src_img, 0, 0);
        } catch (const std::runtime_error &ex) {
            EXPECT_STREQ("Empty image byte", ex.what());
            throw;
        }
    }, std::runtime_error);
}

TEST(resize_image, width_0) {
    const std::string src_img = valid_image_encoded;
    EXPECT_THROW({
        try {
            resize_image(src_img, 0, 100);
        } catch (const std::runtime_error &ex) {
            EXPECT_STREQ("Desired width <= 0", ex.what());
            throw;
        }
    }, std::runtime_error);
}
TEST(resize_image, width_minus) {
    const std::string src_img = valid_image_encoded;
    EXPECT_THROW({
        try {
            resize_image(src_img, -1, 100);
        } catch (const std::runtime_error &ex) {
            EXPECT_STREQ("Desired width <= 0", ex.what());
            throw;
        }
    }, std::runtime_error);
}
TEST(resize_image, height_0) {
    const std::string src_img = valid_image_encoded;
    EXPECT_THROW({
        try {
            resize_image(src_img, 100, 0);
        } catch (const std::runtime_error &ex) {
            EXPECT_STREQ("Desired height <= 0", ex.what());
            throw;
        }
    }, std::runtime_error);
}
TEST(resize_image, height_minus) {
    const std::string src_img = valid_image_encoded;
    EXPECT_THROW({
        try {
            resize_image(src_img, 100, -1);
        } catch (const std::runtime_error &ex) {
            EXPECT_STREQ("Desired height <= 0", ex.what());
            throw;
        }
    }, std::runtime_error);
}

TEST(resize_image, invalid_src) {
    const std::string src_img = " ";
    EXPECT_THROW(resize_image(src_img, 100, 100), std::runtime_error);
}

TEST(resize_image, corrupted_src) {
    const std::string src_img = corruped_image_encoded;
    EXPECT_THROW(resize_image(src_img, 100, 100), std::runtime_error);
}

TEST(resize_image, valid_image) {
    const std::string src_img = valid_image_encoded;
    std::string res;
    EXPECT_NO_THROW(res = resize_image(src_img, 100, 100));
    EXPECT_FALSE(res.empty());
}
