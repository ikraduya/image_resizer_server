#include <gtest/gtest.h>

#include "main.hpp"

TEST(error_body_struct, constructor) {
    // error_body_t body; // deleted function
    error_body_t body(400, "test");
    EXPECT_EQ(body.code, 400);
    EXPECT_EQ(body.message, "test");
}

TEST(validate_json, empty) {
    crow::json::rvalue body;
    const auto ret = validate_json(body);
    EXPECT_TRUE(ret.has_value());
    EXPECT_EQ(ret->code, 400);
    EXPECT_EQ(ret->message, "json body must not be empty");
}

TEST(validate_json, no_input_jpeg) {
    crow::json::rvalue body = crow::json::load("{\
        \"desired_width\": 400,\
        \"desired_height\": 400\
    }");
    const auto ret = validate_json(body);
    EXPECT_TRUE(ret.has_value());
    EXPECT_EQ(ret->code, 400);
    EXPECT_EQ(ret->message, "input_jpeg is required");
}

TEST(validate_json, no_desired_width) {
    crow::json::rvalue body = crow::json::load("{\
        \"input_jpeg\": \"test\",\
        \"desired_height\": 400\
    }");
    const auto ret = validate_json(body);
    EXPECT_TRUE(ret.has_value());
    EXPECT_EQ(ret->code, 400);
    EXPECT_EQ(ret->message, "desired_width is required");
}

TEST(validate_json, no_desired_height) {
    crow::json::rvalue body = crow::json::load("{\
        \"input_jpeg\": \"test\",\
        \"desired_width\": 400\
    }");
    const auto ret = validate_json(body);
    EXPECT_TRUE(ret.has_value());
    EXPECT_EQ(ret->code, 400);
    EXPECT_EQ(ret->message, "desired_height is required");
}

TEST(validate_json, valid_json) {
    crow::json::rvalue body = crow::json::load("{\
        \"input_jpeg\": \"test\",\
        \"desired_width\": 400,\
        \"desired_height\": 400\
    }");
    const auto ret = validate_json(body);
    EXPECT_EQ(ret, std::nullopt);
}
