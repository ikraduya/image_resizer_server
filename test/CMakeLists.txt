include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/refs/tags/release-1.11.0.zip
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

enable_testing()
add_executable(main_test image_util_test.cpp main_test.cpp)
target_include_directories(main_test PRIVATE ${CMAKE_SOURCE_DIR}/src/)
target_link_libraries(main_test PRIVATE
    gtest_main Boost::boost Boost::system Boost::thread ${OpenCV_LIBS})

set_target_properties(main_test
    PROPERTIES
        CXX_STANDARD 17
        CXX_STANDARD_REQUIRED ON
)

include(GoogleTest)
gtest_discover_tests(main_test)
