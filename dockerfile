FROM matimoreyra/opencv:latest AS base
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y remove cmake && \
    apt-get -y install --fix-missing --no-install-recommends \
        apt-transport-https ca-certificates gnupg software-properties-common wget libboost-all-dev && \
    wget -qO - https://apt.kitware.com/keys/kitware-archive-latest.asc | apt-key add - && \
    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' && \
    apt-get -y update && \
    apt-get -y install cmake


FROM base AS test
WORKDIR /test
COPY . .
RUN mkdir -p build && cd build && \
    cmake .. -DCMAKE_BUILD_TYPE="Debug" -DBUILD_TEST=ON && \
    cmake --build .
ENTRYPOINT [ "/test/build/test/main_test" ]

FROM base AS prod
WORKDIR /work
COPY . .
RUN mkdir -p build && cd build && \
    cmake .. -DCMAKE_BUILD_TYPE="Release" && \
    cmake --build . && \
    mkdir -p /app && \
    cp main /app

WORKDIR /app
ENTRYPOINT [ "/app/main" ]
