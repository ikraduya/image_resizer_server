#include "main.hpp"
#include "image_util.hpp"

int main()
{
    crow::SimpleApp app;
#ifdef NDEBUG
    app.loglevel(crow::LogLevel::Warning);
#endif

    CROW_ROUTE(app, "/resize_image")
    .methods("POST"_method)
    ([](const crow::request &req) -> crow::response {
        auto error_response = [](const int code, const std::string &message) -> crow::response {
            crow::json::wvalue ret = {
                {"code", code},
                {"message", message}
            };

            return crow::response(std::move(ret));
        };
        auto x = crow::json::load(req.body);
        
        /* Validate json body */
        auto err_body = validate_json(x);
        if (err_body.has_value()) {
            return error_response(err_body->code, err_body->message);
        }

        /* Parse json body */
        int64_t width = 0, height = 0;
        std::string jpeg;
        try {
            width = x["desired_width"].i();
            height = x["desired_height"].i();
            jpeg = x["input_jpeg"].s();
        } catch (const std::exception &ex) {
            return error_response(400, "error while parsing json body: " + std::string(ex.what()));
        }
        
        /* Resize image */
        std::string resized_jpeg;
        try {
            resized_jpeg = resize_image(std::string(jpeg), width, height);
        } catch (const std::runtime_error &re) {
            return error_response(500, "error while resizing image: " +  std::string(re.what()));
        } catch (...) {
            return error_response(500, "error while resizing image");
        }

        /* Respond with resized image */
        crow::json::wvalue ret = {
            {"code", 200},
            {"message", "success"},
            {"output_jpeg", resized_jpeg}
        };
        return crow::response(std::move(ret));
    });

    app.port(8080).multithreaded().run();
}
