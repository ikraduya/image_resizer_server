#ifndef IMAGE_UTIL_HPP
#define IMAGE_UTIL_HPP

#include <string>
#include <cstdint>
#include <exception>
#include <string>

#include <opencv2/opencv.hpp>

#include "crow_all.h"

/**
 * @brief Convert base64 encoded string to cv image
 * 
 * @param src_image base64 encoded string
 * @return cv::Mat 
 */
cv::Mat base64_to_cv2img(const std::string &src_image) {
    const std::string dec_image = crow::utility::base64decode(src_image);
    const std::vector<uchar> data(dec_image.begin(), dec_image.end());
    cv::Mat img;
    try {
        img = cv::imdecode(cv::Mat(data), cv::IMREAD_COLOR);
    } catch (...) {
        throw std::runtime_error("Error decoding image");
    }
    
    if (img.data == NULL) {
        throw std::runtime_error("Error decoding image");
    }

    return img;
}

/**
 * @brief Convert cv image to base64 encoded string
 * 
 * @param img 
 * @return std::string base64 encoded string
 */
std::string cv2img_to_base64(const cv::Mat &img) {
    std::vector<uchar> buf;
    bool ret = false; 
    try {
        ret = cv::imencode(".jpeg", img, buf);
    } catch (...) {
        throw std::runtime_error("Error encoding back the image");
    }
    if (!ret) {
        throw std::runtime_error("Error encoding back the image");
    }
    auto *buf_ptr = reinterpret_cast<unsigned char*>(buf.data());
    const std::string enc_image = crow::utility::base64encode(buf_ptr, buf.size());
    
    return enc_image;
}

/**
 * @brief Resize base64 encoded image
 * 
 * @param src_image base64 encoded image bytes
 * @param width desired width
 * @param height desired height
 * @return std::string resized base64 encoded image bytes
 */
std::string resize_image(const std::string &src_image, const int64_t width, const int64_t height) {
    if (src_image.empty()) {
        throw std::runtime_error("Empty image byte");
    }
    if (width <= 0) {
        throw std::runtime_error("Desired width <= 0");
    }
    if (height <= 0) {
        throw std::runtime_error("Desired height <= 0");
    }

    cv::Mat img = base64_to_cv2img(src_image);

    cv::Mat resized_img = img;
    try {
        cv::resize(img, resized_img, cv::Size(width, height));
    } catch (...) {
        throw std::runtime_error("Error resizing image");
    }

    std::string enc_image = cv2img_to_base64(resized_img);
    return enc_image;
}

#endif // IMAGE_UTIL_HPP
