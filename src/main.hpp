#ifndef MAIN_HPP
#define MAIN_HPP

#include <optional>
#include <string>

#include "crow_all.h"

/**
 * @brief Struct to store error body messages
 * 
 */
struct error_body_t {
    int code;
    std::string message;
    error_body_t() = delete;
    error_body_t(const int _code, const std::string &_message)
        : code(_code), message(_message) {}
};

/**
 * @brief Validate json body
 * 
 * @param body 
 * @return std::optional<error_body_t> nullopt means json is valid
 */
std::optional<error_body_t> validate_json(const crow::json::rvalue &body) {
    if (!body) {
        return error_body_t{400, "json body must not be empty"};
    }

    if (!body.has("input_jpeg")) {
        return error_body_t{400, "input_jpeg is required"};
    }
    if (!body.has("desired_width")) {
        return error_body_t{400, "desired_width is required"};
    }
    if (!body.has("desired_height")) {
        return error_body_t{400, "desired_height is required"};
    }
    
    return std::nullopt;
}

#endif // MAIN_HPP
