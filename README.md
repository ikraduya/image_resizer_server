# image_resizer_server

**image_resizer_server** is a simple HTTP server that listens on port 8080 to serves a specific function: resizing a jpeg image. The server only serve a single API endpoint below:

`http://[host_address]:8080/resize_image [POST]`

### Input Payload
```json
{
    "input_jpeg": "[base 64 encoded of jpeg binary]",
    "desired_width": "[target_width]",
    "desired_height": "[target_height]"
}
```

### Output Payload
If success:
```json
{
    "code": 200,
    "message": "success",
    "output_jpeg": "[base 64 encoded of resized jpeg binary]"
}
```
If failed:
```json
{
    "code": 4xx/5xx,
    "message": "[error description]"
}
```

## How to Build
### Requirements
- C++ compiler with C++17 support (tested with GCC 9.4.0 and GCC 7.5.0)
- CMake >= 3.10
- Boost library >= 1.65 with Boost::system and Boost::thread
- OpenCV

## Building
```bash
# After git clone
cmake -S . -B build
cmake --build build -j 4
```

## Run
```bash
cd build
./main
```

## Build with Docker
```bash
docker build . -t image_resizer:latest
```
